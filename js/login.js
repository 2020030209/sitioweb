import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import {getDatabase, onValue, ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
    import{getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
  const firebaseConfig = {
    apiKey: "AIzaSyBQRNiEsvNGtEXi0n2FCf5ZomOPKnikC54",
    authDomain: "sneakersjh-665ef.firebaseapp.com",
    projectId: "sneakersjh-665ef",
    storageBucket: "sneakersjh-665ef.appspot.com",
    messagingSenderId: "974999555974",
    appId: "1:974999555974:web:7f7f77a349c032427ae607",
    measurementId: "G-MZEXHQ0YCX"
  };
  const app = initializeApp(firebaseConfig);
  const db= getDatabase();

  var btnLogin= document.getElementById('btnLogin');

  function login(){
    let email = document.getElementById('Usuario').value;
    let password= document.getElementById('contra').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/html/administrador.html";
    // ...
     })
    .catch((error) => {
    location.href="/html/error.html";
    });
  }

  btnLogin.addEventListener('click', login);