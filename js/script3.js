
  // Import the functions you need from the SDKs you need
  import { initializeApp } from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
 import { getFirestore, doc, getDoc, getDocs, collection } from
"https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
 import { getDatabase,onValue,ref,set,get,child,update,remove } from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL} from
"https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyBQRNiEsvNGtEXi0n2FCf5ZomOPKnikC54",
    authDomain: "sneakersjh-665ef.firebaseapp.com",
    projectId: "sneakersjh-665ef",
    storageBucket: "sneakersjh-665ef.appspot.com",
    messagingSenderId: "974999555974",
    appId: "1:974999555974:web:7f7f77a349c032427ae607",
    measurementId: "G-MZEXHQ0YCX"
  };

const app= initializeApp(firebaseConfig);
const db = getDatabase();

 var btnInsertar = document.getElementById("btnInsertar");
 var btnBuscar = document.getElementById("btnBuscar");
 var btnActualizar = document.getElementById("btnActualizar");
 var btnBorrar = document.getElementById("btnBorrar");
 var btnTodos = document.getElementById("btnTodos");
 var productos = document.getElementById("productos");
 var btnLimpiar = document.getElementById('btnLimpiar');
 var archivo= document.getElementById('archivo');
 var verImagen= document.getElementById('verImagen');

// Insertar
var id = "";
var marca = "";
var modelo = "";
var color = "";
var talla = "";
var precio = "";
var status="";
var nombreimg="";
var url="";


 function leerInputs(){
 id = document.getElementById("id").value; 
 marca = document.getElementById("marca").value;
 modelo = document.getElementById("modelo").value;
 color = document.getElementById("color").value;
 talla = document.getElementById('talla').value;
 status=document.getElementById('status').value;
 precio = document.getElementById("precio").value;
 nombreimg= document.getElementById('imgNombre').value;
 url= document.getElementById('URL').value;
 }
function insertDatos(){
leerInputs();
 var talla= document.getElementById("talla").value;
 set(ref(db,'productos/' + id),{
  precio:precio,
  marca:marca,
  modelo: modelo,
  color:color,
  talla:talla,
  Status:status,
  nombreimg: nombreimg,
  url: url})


 .then((docRef) => {
 alert("registro exitoso");
 mostrarProductos();
 console.log("datos" +id+ marca + modelo + color+precio+ talla+url+nombreimg)
 })
 .catch((error) => {
 alert("Error en el registro")
 });


 alert (" se agrego");


};
// mostrar datos
function mostrarProductos(){
const db = getDatabase();
const dbRef = ref(db, 'productos');
onValue(dbRef, (snapshot) => {
 productos.innerHTML=""
 snapshot.forEach((childSnapshot) => {
 const childKey = childSnapshot.key;
 const childData = childSnapshot.val();
 if(childData.Status=="Disponible"){
 productos.innerHTML = "" + productos.innerHTML +" <div class='prod'><center><br><img src="+childData.url +"><br>"+ childKey +" "+ childData.marca +
 " "+childData.modelo + " "+childData.color+"<br>"+"Talla: "+childData.talla +"<br>"+"$"+ childData.precio + "<br></center>  </div>";
 console.log(childKey + ":");
 console.log(childData.marca)
 // ...
}});
}, {
 onlyOnce: true
});
}
function actualizar(){
 leerInputs();
update(ref(db,'productos/'+ id),{
  precio:precio,
  marca:marca,
  modelo: modelo,
  color:color,
  talla:talla,
  Status:status,
  nombreimg: nombreimg,
  url: url
 
}).then(()=>{
 alert("se realizo actualizacion");
 mostrarProductos();
})
.catch(()=>{
 alert("causo Erro " + error );
});
}
function escribirInpust(){
  document.getElementById('marca').value= marca;
  document.getElementById('modelo').value= modelo;
  document.getElementById('color').value= color;
  document.getElementById('talla').value= talla;
  document.getElementById('precio').value= precio;
  document.getElementById('id').value= id;
  document.getElementById('status').value=status;
  document.getElementById('imgNombre').value=nombreimg;
  document.getElementById('URL').value=url;
}
function borrar(){
 leerInputs();
 update(ref(db, 'productos/' + id),{
  Status:"No disponible"
}).then(()=>{
    alert("Se deshabilitó");
    mostrarProductos();
    limpiar();
}).catch(() => {
    alert("Surgio un error");
});



}
function mostrarDatos(){
 leerInputs();
 console.log("mostrar datos ");
 const dbref = ref(db);
 get(child(dbref,'productos/'+ id)).then((snapshot)=>{
 if(snapshot.exists()){
  status= snapshot.val().Status;
  marca = snapshot.val().marca;
  precio = snapshot.val().precio;
  modelo = snapshot.val().modelo;
  color = snapshot.val().color;
  talla = snapshot.val().talla;
  nombreimg= snapshot.val().nombreimg;
  url= snapshot.val().url;
 console.log(talla);
 escribirInpust();
}
 else {

 alert("No existe");
 }
}).catch((error)=>{
 alert("error buscar" + error);
});
}
function limpiar(){
  productos.innerHTML="";
  marca="";
  modelo="";
  color="";
  talla=1;
  nombreimg="";
  url="";
 escribirInpust();
}
function cargarImagen(){
  const file= event.target.files[0];
  const name= event.target.files[0].name;

  const storage= getStorage();
  const storageRef= refS(storage, 'imagenes/' + name);

  uploadBytes(storageRef, file).then((snapshot) => {
    document.getElementById('imgNombre').value=name;

    alert('se cargo la imagen');
  });
}

function descargarImagen(){
  archivo= document.getElementById('imgNombre').value;
  // Create a reference to the file we want to download
const storage = getStorage();
const starsRef = refS(storage, 'imagenes/' + archivo);

// Get the download URL
getDownloadURL(starsRef)
  .then((url) => {
   document.getElementById('URL').value=url;
   document.getElementById('imagen').src=url;
  })
  .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/object-not-found':
        console.log("No existe el archivo");
        break;
      case 'storage/unauthorized':
        console.log("No tiene permisos");
        break;
      case 'storage/canceled':
        console.log("No existe conexion con la base de datos")
        break;

      // ...

      case 'storage/unknown':
        console.log("Ocurrio algo inesperado")
        break;
    }
  });
}
btnInsertar.addEventListener('click',insertDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnBorrar.addEventListener('click',borrar);
btnTodos.addEventListener('click', mostrarProductos);
btnLimpiar.addEventListener('click', limpiar);
archivo.addEventListener('change', cargarImagen);
verImagen.addEventListener('click', descargarImagen); 